package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Faq(
    @PrimaryKey
    @SerializedName("id") val id: Int?,
    @SerializedName("jenis") val jenis: String?,
    @SerializedName("judul") val judul: String?,
    @SerializedName("deskripsi") val deskripsi: String?,
    @SerializedName("tampil") val tampil: String?,
    @SerializedName("user_id") val userId: Int?,
    @SerializedName("urutan") val urutan: Int?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?
) : Serializable