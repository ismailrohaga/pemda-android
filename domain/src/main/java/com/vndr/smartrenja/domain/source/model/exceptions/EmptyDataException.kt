package com.vndr.smartrenja.domain.source.model.exceptions

import java.lang.Exception

class EmptyDataException: Exception()