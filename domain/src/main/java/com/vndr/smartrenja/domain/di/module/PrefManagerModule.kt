package com.vndr.smartrenja.domain.di.module

import android.content.Context
import com.vndr.smartrenja.domain.source.local.PrefManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object PrefManagerModule {
    @Provides
    @Singleton
    fun providePrefManager(context: Context): PrefManager = PrefManager(context)
}