package com.vndr.smartrenja.domain.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.vndr.smartrenja.domain.di.qualifier.RetrofitAuthQualifier
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import com.vndr.smartrenja.domain.source.remote.AuthInterceptor
import com.vndr.smartrenja.domain.source.remote.NetworkInterceptor
import com.vndr.smartrenja.domain.source.remote.RequestInterceptor
import com.vndr.smartrenja.domain.util.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object RetrofitModule {
    @Provides
    @Singleton
    fun provideBaseUrl(): String = BASE_URL

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideAuthInterceptor(
        tokenDao: TokenDao
    ): AuthInterceptor = AuthInterceptor(tokenDao)

    @Provides
    @Singleton
    fun provideRequestInterceptor(): RequestInterceptor = RequestInterceptor()

    @Provides
    @Singleton
    fun provideNetworkInterceptor(): NetworkInterceptor = NetworkInterceptor()

    @Provides
    @Singleton
    @RetrofitAuthQualifier
    fun getAuthHttpClient(
        requestInterceptor: RequestInterceptor,
        networkInterceptor: NetworkInterceptor,
        authInterceptor: AuthInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .addInterceptor(requestInterceptor)
            .addInterceptor(authInterceptor)
            .addNetworkInterceptor(networkInterceptor)
            .readTimeout(100, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .build()

    @Provides
    @RetrofitAuthQualifier
    @Singleton
    fun provideRetrofitAuth(
        baseUrl: String,
        gson: Gson,
        @RetrofitAuthQualifier client: OkHttpClient,
    ): Retrofit = Retrofit.Builder().baseUrl(baseUrl)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create()).build()
}