package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.time.LocalDate

@Keep
data class Kunker(
    @SerializedName("id") val id: Int?,
    @SerializedName("rencana_kerja_id") val rencanaKerjaId: String?,
    @SerializedName("rencana_kerja") val rencanaKerja: String?,
    @SerializedName("program_kerja_id") val programKerjaId: Int?,
    @SerializedName("program_kerja") val programKerja: String?,
    @SerializedName("tema_kegiatan") val temaKegiatan: String?,
    @SerializedName("gambaran_umum") val gambaranUmum: String?,
    @SerializedName("lokasi_tujuan") val lokasiTujuan: String?,
    @SerializedName("tanggal_mulai") val tanggalMulai: String?,
    @SerializedName("tanggal_akhir") val tanggalAkhir: String?,
    @SerializedName("pimpinan_kunker") val pimpinanKunker: String?,
    @SerializedName("sekertariat") val sekretariat: String?,
    @SerializedName("tanggal") val tanggal: String?,
    @SerializedName("notulen") val notulen: String?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("is_approve_laporan_kunker") val isApproveLaporanKunker: Boolean?,
    @SerializedName("is_approve_materi_kunker") val isApproveMateriKunker: Boolean?,
    @SerializedName("status_laporan_kunker") val statusLaporanKunker: String?,
    @SerializedName("status_materi_kunker") val statusMateriKunker: String?,
    @SerializedName("catatan_perbaikan_laporan_kunker") val catatanLaporanKunker: String?,
    @SerializedName("catatan_perbaikan_materi_kunker") val catatanMateriKunker: String?,
    @SerializedName("peserta_kunkers") val peserta: List<Peserta>?,
    @SerializedName("galleries") val galleries: List<Gallery>?,
    @SerializedName("files") val files: Files?,
    val date: LocalDate?
) : Serializable