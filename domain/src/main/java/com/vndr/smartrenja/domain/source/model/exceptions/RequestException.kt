package com.vndr.smartrenja.domain.source.model.exceptions

import com.vndr.smartrenja.domain.source.model.response.ServerErrorResponse
import retrofit2.HttpException

class RequestException : Throwable {
    var errorHttp: ServerErrorResponse? = null

    constructor() : super()

    constructor(errorResponse: ServerErrorResponse) : super() {
        errorHttp = errorResponse
    }

    constructor(throwable: Throwable) : super(throwable) {
        if (throwable is HttpException && throwable.code() >= 400) {
            errorHttp = ServerErrorResponse.create(throwable.response()?.errorBody()?.string())
        }
    }
}