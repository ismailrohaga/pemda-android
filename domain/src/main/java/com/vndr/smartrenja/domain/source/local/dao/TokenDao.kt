package com.vndr.smartrenja.domain.source.local.dao

import androidx.room.*
import com.vndr.smartrenja.domain.source.model.Token
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
abstract class TokenDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: Token): Long

    @Query("DELETE FROM token_table")
    abstract fun clear()

    @Query("DELETE FROM token_table")
    abstract fun delete(): Completable

    @Query("SELECT * FROM token_table LIMIT 1")
    abstract fun getToken(): Single<Token>

    @Query("SELECT * FROM token_table LIMIT 1")
    abstract fun getTokenData(): Token?

    @Transaction
    open fun deleteAndInsertAccessToken(token: Token) {
        clear()
        insert(Token(token.token))
    }
}