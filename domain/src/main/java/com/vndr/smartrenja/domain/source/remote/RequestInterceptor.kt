package com.vndr.smartrenja.domain.source.remote

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class RequestInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val request = original.newBuilder()
            .header("Accept", "application/json")
            .method(original.method, original.body)

        return chain.proceed(request.build())
    }
}