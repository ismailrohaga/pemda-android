package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.time.LocalDate

@Keep
data class Schedule(
    @PrimaryKey
    @SerializedName("schedule_id") val id: Int?,
    @SerializedName("kegiatan_kerja") val kegiatanKerja: String?,
    @SerializedName("program_kerja") val programKerja: String?,
    @SerializedName("tanggal") val tanggal: String?,
    @SerializedName("tema_rapat") val temaRapat: String?,
    @SerializedName("tema_kunker") val temaKunker: String?,
    @SerializedName("dasar_reses") val dasarReses: String?,
    @SerializedName("rapat_id") val rapatId: Int?,
    @SerializedName("kunker_id") val kunkerId: Int?,
    @SerializedName("reses_id") val resesId: Int?,
    val date: LocalDate
)