package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.Anggota
import com.vndr.smartrenja.domain.model.Faq
import com.vndr.smartrenja.domain.model.Faqs
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class FaqsRepository @Inject constructor(
    private val apiService: ApiService,
) : BaseRepository() {
    fun getFaqs(): Single<Resource<List<Faqs>>> = apiService.getFaq()
        .prepareNetworkRequest()
        .addSchedulers()

    fun getTerms(): Single<Resource<List<Faqs>>> = apiService.getTerms()
        .prepareNetworkRequest()
        .addSchedulers()
}

