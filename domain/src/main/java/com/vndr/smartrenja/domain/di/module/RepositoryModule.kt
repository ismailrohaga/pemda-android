package com.vndr.smartrenja.domain.di.module

import com.google.gson.Gson
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import com.vndr.smartrenja.domain.source.local.dao.UserDao
import com.vndr.smartrenja.domain.source.remote.ApiService
import com.vndr.smartrenja.domain.source.repo.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object RepositoryModule {
    @Provides
    @Singleton
    fun provideAnggotaRepository(apiService: ApiService): AnggotaRepository =
        AnggotaRepository(apiService)

    @Provides
    @Singleton
    fun provideApprovalRepository(apiService: ApiService): ApprovalRepository =
        ApprovalRepository(apiService)

    @Provides
    @Singleton
    fun provideAuthRepository(
        gson: Gson,
        apiService: ApiService,
        prefManager: PrefManager,
        userDao: UserDao,
        tokenDao: TokenDao,
    ): AuthRepository = AuthRepository(gson, apiService, prefManager, userDao, tokenDao)

    @Provides
    @Singleton
    fun provideBannerRepository(apiService: ApiService): BannerRepository =
        BannerRepository(apiService)

    @Provides
    @Singleton
    fun provideFaqsRepository(apiService: ApiService): FaqsRepository =
        FaqsRepository(apiService)

    @Provides
    @Singleton
    fun provideKegiatanRepository(apiService: ApiService): KegiatanRepository =
        KegiatanRepository(apiService)

    @Provides
    @Singleton
    fun provideNotificationRepository(apiService: ApiService): NotificationRepository =
        NotificationRepository(apiService)

    @Provides
    @Singleton
    fun provideUserRepository(
        apiService: ApiService, userDao: UserDao,
        tokenDao: TokenDao,
    ): UserRepository =
        UserRepository(apiService, userDao, tokenDao)
}