package com.vndr.smartrenja.domain.source.model.state

import android.util.Log
import androidx.annotation.StringRes
import androidx.room.rxjava3.EmptyResultSetException
import com.vndr.smartrenja.domain.R
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

sealed class Resource<out T> {
    object Loading : Resource<Nothing>()
    data class Success<out R>(val data: R?) : Resource<R>()

    sealed class Error : Resource<Nothing>() {
        class ApiError(val message: String, val errorCode: Int) : Error() {
            override fun getUiMessage(): Int {
                Log.e(this::class.java.name, "---> Resource.Error.ApiError: $errorCode")
                return when (errorCode) {
                    else -> R.string.error_unexpected
                }
            }

            override fun getStringMessage(): String = message
        }

        class Thrown(val exception: Throwable) : Error() {
            override fun getUiMessage(): Int {
                Log.e(this::class.java.name, "---> Resource.Error.Thrown: $exception")
                return when (exception) {
                    is UnknownHostException, is SocketTimeoutException, is ConnectException -> R.string.error_no_internet
                    is EmptyResultSetException -> R.string.error_unexpected//Query returned empty result set
                    else -> R.string.error_unexpected
                }
            }

            override fun getStringMessage(): String {
                return "Unexpected"
            }
        }

        @StringRes
        abstract fun getUiMessage(): Int

        abstract fun getStringMessage(): String
    }
}