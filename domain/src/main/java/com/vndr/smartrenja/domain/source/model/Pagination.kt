package com.vndr.smartrenja.domain.source.model

import com.google.gson.annotations.SerializedName

data class Pagination(
    @SerializedName("current_page") val currentPage: Int,
    @SerializedName("current_count") val currentCount: Int,
    @SerializedName("total_count") val totalCount: Int,
)
