package com.vndr.smartrenja.domain.source.repo

import com.vndr.smartrenja.domain.base.BaseRepository
import com.vndr.smartrenja.domain.model.Banner
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.remote.ApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class BannerRepository @Inject constructor(
    private val apiService: ApiService,
) : BaseRepository() {
    fun getBanner(): Single<Resource<List<Banner>>> = apiService.getBanner()
        .prepareNetworkRequest()
        .addSchedulers()
}

