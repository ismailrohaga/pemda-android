package com.vndr.smartrenja.domain.source.model.response

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

class ServerErrorResponse(@SerializedName("code") val errorCode: Int) {
    companion object {
        fun create(stringError: String?): ServerErrorResponse =
            Gson().fromJson(stringError, ServerErrorResponse::class.java)
    }
}
