package com.vndr.smartrenja.domain.util

import android.util.Base64
import java.text.DecimalFormat

object StringUtils {

    fun randomStringBase64(): String {
        val item = (List(32) {
            (('a'..'z') + ('A'..'Z') + ('0'..'9')).random()
        }.joinToString(""))
        return Base64.encodeToString(item.toByteArray(),Base64.NO_PADDING).trim()
    }


    fun numberFormat(amount: String?, format: String? = "###,###,###"): String? {
        return if (!amount.isNullOrBlank()){
            DecimalFormat(format).format(
                amount.toDouble()
            )
        }else{
            amount
        }
    }

}