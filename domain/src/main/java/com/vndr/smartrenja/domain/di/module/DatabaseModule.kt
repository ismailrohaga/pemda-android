package com.vndr.smartrenja.domain.di.module

import android.content.Context
import androidx.room.Room
import com.vndr.smartrenja.domain.source.local.SmartRenjaDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(context: Context): SmartRenjaDb =
            Room.databaseBuilder(context, SmartRenjaDb::class.java, SmartRenjaDb.DB_NAME)
                    .fallbackToDestructiveMigration().build()
}