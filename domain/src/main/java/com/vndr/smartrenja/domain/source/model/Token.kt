package com.vndr.smartrenja.domain.source.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "token_table")
data class Token(
    @PrimaryKey
    @ColumnInfo(name = "token")
    @SerializedName("token")
    val token: String,
)