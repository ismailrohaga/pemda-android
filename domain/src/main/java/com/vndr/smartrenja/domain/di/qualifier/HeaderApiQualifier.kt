package com.vndr.smartrenja.domain.di.qualifier

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class HeaderApiQualifier