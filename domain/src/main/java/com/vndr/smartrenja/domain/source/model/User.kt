package com.vndr.smartrenja.domain.source.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.vndr.smartrenja.domain.model.Akd
import com.vndr.smartrenja.domain.model.Jabatan
import com.vndr.smartrenja.domain.model.ProgramKerja

@Entity(tableName = "user_table")
data class User constructor(
    @PrimaryKey
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String?,
    @SerializedName("email") val email: String?,
    @SerializedName("username") val username: String?,
    @SerializedName("photo") val photo: String?,
    @SerializedName("file_renja_dprd") val fileRenjaDprd: String?,
    @SerializedName("email_verified_at") val emailVerifiedAt: String?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("akd") val akd: List<Akd>?,
    @SerializedName("jabatan") val jabatan: List<Jabatan>?
) {
    class DataConverters {
        @TypeConverter
        fun akdFromJson(json: String?): List<Akd>? {
            return if (!json.isNullOrEmpty()) Gson().fromJson(json,
                object : TypeToken<List<Akd>>() {}.type) else null
        }

        @TypeConverter
        fun akdToJson(list: List<Akd>?): String? {
            return if (!list.isNullOrEmpty()) Gson().toJson(list) else null
        }

        @TypeConverter
        fun jabatanFromJson(json: String?): List<Jabatan>? {
            return if (!json.isNullOrEmpty()) Gson().fromJson(json,
                object : TypeToken<List<Jabatan>>() {}.type) else null
        }

        @TypeConverter
        fun jabatanToJson(list: List<Jabatan>?): String? {
            return if (!list.isNullOrEmpty()) Gson().toJson(list) else null
        }

        @TypeConverter
        fun programKerjaFromJson(json: String?): List<ProgramKerja>? {
            return if (!json.isNullOrEmpty()) Gson().fromJson(json,
                object : TypeToken<List<ProgramKerja>>() {}.type) else null
        }

        @TypeConverter
        fun programKerjaToJson(list: List<ProgramKerja>?): String? {
            return if (!list.isNullOrEmpty()) Gson().toJson(list) else null
        }
    }
}
