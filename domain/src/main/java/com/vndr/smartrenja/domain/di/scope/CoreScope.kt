package com.vndr.smartrenja.domain.di.scope

import javax.inject.Scope


@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class CoreScope