package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Anggota(
    @SerializedName("nama") val nama: String?,
    @SerializedName("jabatan") val jabatan: String?,
    @SerializedName("photo") val photo: String
) : Serializable