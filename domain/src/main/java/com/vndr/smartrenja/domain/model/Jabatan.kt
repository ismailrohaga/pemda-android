package com.vndr.smartrenja.domain.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Keep
data class Jabatan(
    @SerializedName("id") val id: Int?,
    @SerializedName("nama_jabatan") val namaJabatan: String?,
    @SerializedName("akd") val akd: String?
) : Serializable