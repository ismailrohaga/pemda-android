package com.vndr.smartrenja.domain.di.module

import com.vndr.smartrenja.domain.source.local.SmartRenjaDb
import com.vndr.smartrenja.domain.source.local.dao.TokenDao
import com.vndr.smartrenja.domain.source.local.dao.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DaoModule {
    @Provides
    @Singleton
    fun provideTokenDao(smartRenjaDb: SmartRenjaDb): TokenDao = smartRenjaDb.getAccessTokenDao()

    @Provides
    @Singleton
    fun provideAccountDao(smartRenjaDb: SmartRenjaDb): UserDao = smartRenjaDb.getAccountDao()
}