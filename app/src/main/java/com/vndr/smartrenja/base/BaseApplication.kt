package com.vndr.smartrenja.base

import android.app.Activity
import android.app.Application
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.util.CoilUtils
import com.google.firebase.messaging.FirebaseMessaging
import com.vndr.smartrenja.domain.di.component.CoreComponent
import com.vndr.smartrenja.domain.di.component.DaggerCoreComponent
import com.vndr.smartrenja.util.NotificationHelper
import okhttp3.OkHttpClient
import java.util.*

class BaseApplication : Application(), ImageLoaderFactory {
    private val lifecycleCallbacks by lazy { LifecycleCallbacks() }

    override fun onCreate() {
        super.onCreate()
        Log.d(this.javaClass.name, "onCreate: ${this.packageName}")
        appComponent = DaggerCoreComponent.factory().create(this)

        registerActivityLifecycleCallbacks(lifecycleCallbacks)
        registerNotificationChannel()

        // cache the uuid and fcm
        appComponent?.let {
            if (it.prefManager().getDeviceUuid() == null)
                it.prefManager().setDeviceUUid(UUID.randomUUID().toString())

            if (it.prefManager().getFirebaseToken() == null)
                FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                    if (!task.isSuccessful) return@addOnCompleteListener // failed to fetch fcm
                    it.prefManager().setFirebaseToken(task.result)
                    Log.d(this::class.simpleName, "++> fcm: ${task.result}")
                }
            Log.d(this::class.simpleName, "++> fcm: ${it.prefManager().getFirebaseToken()}")
        }
    }

    override fun onTerminate() {
        unregisterActivityLifecycleCallbacks(lifecycleCallbacks)
        super.onTerminate()
    }

    override fun newImageLoader() = ImageLoader.Builder(applicationContext)
        .crossfade(true)
        .okHttpClient {
            OkHttpClient.Builder().cache(CoilUtils.createDefaultCache(applicationContext)).build()
        }.build()

    private fun registerNotificationChannel() {
        // register notification channel here

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // default channel
            NotificationHelper.createNotificationChannel(
                this,
                NotificationManagerCompat.IMPORTANCE_DEFAULT,
                true,
                "default",
                "General Notification Channel."
            )

            // reminder channel
            NotificationHelper.createNotificationChannel(
                this,
                NotificationManagerCompat.IMPORTANCE_HIGH,
                true,
                "reminder",
                "Reminder Notification Channel."
            )
        }
    }


    companion object {
        @JvmStatic
        var appComponent: CoreComponent? = null
    }

    private class LifecycleCallbacks : ActivityLifecycleCallbacks {
        override fun onActivityCreated(p0: Activity, p1: Bundle?) {}
        override fun onActivityStarted(p0: Activity) {}
        override fun onActivityResumed(activity: Activity) {}
        override fun onActivityPaused(activity: Activity) {}
        override fun onActivityStopped(p0: Activity) {}
        override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {}
        override fun onActivityDestroyed(p0: Activity) {}
    }

}