package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemScheduleBinding
import com.vndr.smartrenja.domain.model.Schedule
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.layoutInflater
import com.vndr.smartrenja.ui.activities.KegiatanDetailActivity
import com.vndr.smartrenja.util.TimeConverter
import java.util.ArrayList

class UpcomingScheduleAdapter : RecyclerView.Adapter<UpcomingScheduleAdapter.ViewHolder>() {
    private var events: MutableList<Schedule> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemScheduleBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return events.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(list: List<Schedule>) {
        events.clear()
        for (i in list) {
            events.add(i)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val data = events[position]
        when {
            data.rapatId != null -> {
                viewHolder.bindRapat(data)
            }
            data.kunkerId != null -> {
                viewHolder.bindKunker(data)
            }
            else -> {
                viewHolder.bindReses(data)
            }
        }

        if (position == events.lastIndex) {
            val params = viewHolder.itemView.layoutParams as RecyclerView.LayoutParams
            params.marginEnd = 32
            viewHolder.itemView.layoutParams = params
        }
    }

    inner class ViewHolder(private val binding: ItemScheduleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            itemView.setOnClickListener {
                val data = events[bindingAdapterPosition]
                when {
                    data.rapatId != null -> {
                        it.context.startActivity(
                            Intent(it.context, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RAPAT)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, data.rapatId)
                                .putExtra("action_request", "null")
                        )
                    }
                    data.kunkerId != null -> {
                        it.context.startActivity(
                            Intent(it.context, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.KUNKER)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, data.kunkerId)
                                .putExtra("action_request", "null")
                        )
                    }
                    else -> {
                        it.context.startActivity(
                            Intent(it.context, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RESES)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, data.resesId)
                                .putExtra("action_request", "null")
                        )
                    }
                }
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        fun bindRapat(schedule: Schedule) {
            binding.textViewName.text = schedule.temaRapat
            binding.textViewType.text = "Rapat"
            binding.textViewDate.text =
                schedule.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.layoutSchedule.background =
                binding.root.context.getDrawable(R.drawable.bg_rounded_gradient_red)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        fun bindKunker(schedule: Schedule) {
            binding.textViewName.text = schedule.temaKunker
            binding.textViewType.text = "Kunker"
            binding.textViewDate.text =
                schedule.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.layoutSchedule.background =
                binding.root.context.getDrawable(R.drawable.bg_rounded_gradient_orange)
        }

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n")
        fun bindReses(schedule: Schedule) {
            binding.textViewName.text = schedule.dasarReses
            binding.textViewType.text = "Reses"
            binding.textViewDate.text =
                schedule.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.layoutSchedule.background =
                binding.root.context.getDrawable(R.drawable.bg_rounded_gradient_red)
        }
    }
}