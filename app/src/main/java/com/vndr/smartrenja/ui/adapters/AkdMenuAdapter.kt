package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vndr.smartrenja.databinding.ItemAkdBinding
import com.vndr.smartrenja.domain.model.Akd
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.layoutInflater
import com.vndr.smartrenja.extention.serviceInjection
import com.vndr.smartrenja.ui.activities.AkdActivity
import com.vndr.smartrenja.util.toJson
import java.util.ArrayList
import javax.inject.Inject

class AkdMenuAdapter : RecyclerView.Adapter<AkdMenuAdapter.ViewHolder>() {
    @Inject
    lateinit var prefManager: PrefManager

    private var data: MutableList<Akd> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        serviceInjection.inject(this)
        return ViewHolder(
            ItemAkdBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(list: List<Akd>) {
        data.clear()
        for (i in list) {
            data.add(i)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(private val binding: ItemAkdBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: Akd) {
            binding.textViewName.text = data.namaAkd

            itemView.setOnClickListener {
                it.context.startActivity(
                    Intent(it.context, AkdActivity::class.java)
                        .putExtra(Constants.INTENT_AKD_ID, data.akdId)
                        .putExtra(Constants.INTENT_AKD_NAME, data.namaAkd)
                )
                prefManager.setMenuAkd(data.toJson())
            }
        }
    }
}
