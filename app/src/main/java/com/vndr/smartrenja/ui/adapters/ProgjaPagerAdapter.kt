@file:Suppress("DEPRECATION")

package com.vndr.smartrenja.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.vndr.smartrenja.domain.util.Constants.PELAKSANAAN
import com.vndr.smartrenja.domain.util.Constants.PELAPORAN
import com.vndr.smartrenja.domain.util.Constants.PERENCANAAN
import com.vndr.smartrenja.domain.util.Constants.TYPE_ALL
import com.vndr.smartrenja.ui.fragments.DataFragment

class ProgjaPagerAdapter(fm: FragmentManager, programKerjaId: Int, canApprove: Boolean) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val pages = listOf(
        DataFragment.newInstance(PERENCANAAN, TYPE_ALL, programKerjaId, canApprove),
        DataFragment.newInstance(PELAKSANAAN, TYPE_ALL, programKerjaId, canApprove),
        DataFragment.newInstance(PELAPORAN, TYPE_ALL, programKerjaId, canApprove)
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> PERENCANAAN
            1 -> PELAKSANAAN
            else -> PELAPORAN
        }
    }
}