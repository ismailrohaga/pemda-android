package com.vndr.smartrenja.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivityMainBinding
import com.vndr.smartrenja.domain.source.local.PrefManager
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.activityInjection
import com.vndr.smartrenja.extention.makeGone
import com.vndr.smartrenja.extention.makeVisible
import com.vndr.smartrenja.util.setOnSafeClickListener
import com.vndr.smartrenja.util.toJson
import com.vndr.smartrenja.viewmodel.MainViewModel
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var prefManager: PrefManager

    private val navController by lazy {
        (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
            .navController
    }

    private val mainViewModel: MainViewModel by viewModels()
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    private var id: Int? = null
    private var type: String? = null
    private var action: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        activityInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.bottomNav.itemIconTintList = null
        binding.bottomNav.setupWithNavController(navController)

        mainViewModel.loadBanners()
        mainViewModel.loadSchedules(null, null, null)
        mainViewModel.loadUpcomingSchedules()
        mainViewModel.loadNotifications()
        mainViewModel.loadFaqs()
        mainViewModel.loadTerms()

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.notification_fragment)
                binding.materialButtonReadAll.makeVisible()
            else binding.materialButtonReadAll.makeGone()
        }

        binding.materialButtonReadAll.setOnSafeClickListener {
            mainViewModel.clearNotifications()
        }

        val bundle = intent.extras
        if (bundle != null) {
            id = bundle.getString("id")?.toInt()
            type = bundle.getString("type")
            action = bundle.getString("action_request")
            bundle.getString("notif_id")?.let { mainViewModel.readNotification(it) }
            if (bundle.getString("akd") != null) prefManager.setMenuAkd(
                bundle.getString("akd").toJson()
            )

            if (type != null) {
                when (type) {
                    "rapat" -> {
                        type = null
                        startActivity(
                            Intent(this, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RAPAT)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, id)
                                .putExtra("action_request", action)
                        )
                    }
                    "kunker" -> {
                        type = null
                        startActivity(
                            Intent(this, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.KUNKER)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, id)
                                .putExtra("action_request", action)
                        )
                    }
                    "reses" -> {
                        type = null
                        startActivity(
                            Intent(this, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RESES)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, id)
                                .putExtra("action_request", action)
                        )
                    }
                }
                id = null
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // User continuous update
        mainViewModel.fetchUser()
        mainViewModel.loadSchedules(null, null, null)
        mainViewModel.loadUpcomingSchedules()
    }
}