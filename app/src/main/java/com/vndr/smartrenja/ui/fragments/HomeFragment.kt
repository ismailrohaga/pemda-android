package com.vndr.smartrenja.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.FragmentHomeBinding
import com.vndr.smartrenja.domain.model.Jabatan
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.extention.makeInVisible
import com.vndr.smartrenja.extention.makeVisible
import com.vndr.smartrenja.extention.toast
import com.vndr.smartrenja.ui.activities.ResesActivity
import com.vndr.smartrenja.ui.adapters.AkdMenuAdapter
import com.vndr.smartrenja.ui.adapters.BannerAdapter
import com.vndr.smartrenja.ui.adapters.UpcomingScheduleAdapter
import com.vndr.smartrenja.viewmodel.MainViewModel

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val mainViewModel: MainViewModel by activityViewModels()

    private val bannerAdapter by lazy { BannerAdapter() }
    private val upcomingScheduleAdapter by lazy { UpcomingScheduleAdapter() }
    private val akdMenuAdapter by lazy { AkdMenuAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.sliderViewBanner.setSliderAdapter(bannerAdapter)
        binding.sliderViewBanner.setIndicatorAnimation(IndicatorAnimationType.WORM)
        binding.sliderViewBanner.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        binding.sliderViewBanner.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        binding.sliderViewBanner.indicatorSelectedColor = Color.BLACK
        binding.sliderViewBanner.indicatorUnselectedColor = Color.GRAY
        binding.recyclerviewUpcomingSchedule.adapter = upcomingScheduleAdapter
        binding.recyclerviewGridAkd.adapter = akdMenuAdapter

        mainViewModel.userLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                    binding.textViewEmptyAkd.text = getString(R.string.label_loading)
                    binding.layoutEmptySchedule.makeVisible()
                }
                is Resource.Success -> resource.data?.let {
                    binding.textViewUserName.text = "Halo, ${it.name}"
                    binding.textViewUserPosition.text =
                        it.jabatan?.let { it1 -> bindJabatan(it1) }
                    binding.imageViewUserPicture.load(it.photo) {
                        scale(Scale.FILL)
                        transformations(RoundedCornersTransformation(12f))
                    }

                    if (it.akd?.isNotEmpty() == true) {
                        it.akd?.let { it1 -> akdMenuAdapter.addAll(it1) }
                        binding.layoutEmptySchedule.makeInVisible()
                    } else {
                        binding.textViewEmptyAkd.text = getString(R.string.label_not_found)
                        binding.layoutEmptySchedule.makeVisible()
                    }
                }
                is Resource.Error -> {
                    binding.textViewEmptyAkd.text = getString(R.string.label_not_found)
                    binding.layoutEmptySchedule.makeVisible()
                }
            }
        })

        mainViewModel.upcomingScheduleListLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                    binding.textViewEmptySchedule.text = getString(R.string.label_loading)
                    binding.layoutEmptySchedule.makeVisible()
                }
                is Resource.Success -> resource.data?.let {
                    upcomingScheduleAdapter.addAll(it)
                    binding.layoutEmptySchedule.makeInVisible()
                }
                is Resource.Error -> {
                    binding.textViewEmptySchedule.text = getString(R.string.label_not_found)
                    binding.layoutEmptySchedule.makeVisible()
                }
            }
        })

        mainViewModel.bannersLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> {
                    resource.data?.let { bannerAdapter.addAll(it) }
                }
                is Resource.Error.ApiError -> {
                    toast { resource.message }
                }
                is Resource.Error.Thrown -> {
                    toast { resource.exception.message.toString() }
                }
            }
        })

        binding.cardViewReses.setOnClickListener {
            context?.startActivity(Intent(context, ResesActivity::class.java))
        }
    }

    private fun bindJabatan(jabatan: List<Jabatan>): String {
        var i = 0
        val s = StringBuilder()
        jabatan.forEach { j ->
            s.append("${j.namaJabatan} ${j.akd}")
            i++
            if (jabatan.size != i) s.append(", ")
        }
        return s.toString()
    }
}