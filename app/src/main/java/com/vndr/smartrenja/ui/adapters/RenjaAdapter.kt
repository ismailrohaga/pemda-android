package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemRenjaBinding
import com.vndr.smartrenja.domain.model.Renja
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.domain.util.Constants.PELAPORAN
import com.vndr.smartrenja.domain.util.Constants.PERENCANAAN
import com.vndr.smartrenja.extention.layoutInflater
import com.vndr.smartrenja.extention.makeGone
import com.vndr.smartrenja.extention.makeVisible
import com.vndr.smartrenja.ui.activities.KegiatanActivity
import java.util.*

class RenjaAdapter(private val type: String, private val progjaId: Int) :
    RecyclerView.Adapter<RenjaAdapter.EventsViewHolder>(), Filterable {

    var onItemClick: ((Renja) -> Unit)? = null
    val list = mutableListOf<Renja>()
    var filterList = mutableListOf<Renja>()

    init {
        filterList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        return EventsViewHolder(
            ItemRenjaBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: EventsViewHolder, position: Int) {
        val data = filterList[position]
        when (type) {
            PERENCANAAN -> {
                viewHolder.bindPerencanaan(data, progjaId)
            }
            PELAPORAN -> {
                viewHolder.bindPelaporan(data, progjaId)
            }
        }

        if (position == filterList.lastIndex) {
            val params = viewHolder.itemView.layoutParams as RecyclerView.LayoutParams
            params.bottomMargin = 32
            viewHolder.itemView.layoutParams = params
        }
    }

    override fun getItemCount(): Int = filterList.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString().toLowerCase(Locale.ROOT).replace(" ", "")
                filterList = if (charSearch.isEmpty()) {
                    list
                } else {
                    val resultList = mutableListOf<Renja>()

                    list.filter {
                        it.nama?.toLowerCase(Locale.ROOT)?.replace(" ", "")
                            ?.contains(charSearch) == true
                    }.apply {
                        resultList.addAll(this)
                    }

                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = filterList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterList = results?.values as MutableList<Renja>
                notifyDataSetChanged()
            }

        }
    }

    @SuppressLint("SetTextI18n")
    inner class EventsViewHolder(private val binding: ItemRenjaBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindPerencanaan(renja: Renja, progjaId: Int) {
            binding.groupPerencanaan.makeVisible()
            binding.textViewRenjaTitle.text = renja.nama
            binding.textViewVolume.text = "Volume: ${renja.volume}"
            binding.textViewIndikator.text = "Indikator: ${renja.indikator}"
            binding.textViewTarget.text = "Target: ${renja.target}"
            binding.imageViewRenja.setImageResource(bindByProgjaId(progjaId)[0])
            binding.constraintLayoutRenja.background =
                binding.root.context.getDrawable(bindByProgjaId(progjaId)[1])

            binding.buttonDetail.makeGone()
            binding.buttonLaporan.makeGone()
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindPelaporan(renja: Renja, progjaId: Int) {
            binding.groupPelaporan.makeVisible()
            binding.textViewRenjaTitleFlip.text = renja.nama
            binding.imageViewRenjaFlip.setImageResource(bindByProgjaId(progjaId)[0])
            binding.constraintLayoutRenja.background =
                binding.root.context.getDrawable(bindByProgjaId(progjaId)[1])

            if (progjaId == 3 || progjaId == 4) binding.imageViewRenjaFlip.scaleX = (-1).toFloat()

            binding.buttonDetail.makeVisible()
            if (!renja.fileLaporanProgja.isNullOrBlank()) binding.buttonLaporan.makeVisible()

            binding.buttonDetail.setOnClickListener {
                it.context.startActivity(
                    Intent(it.context, KegiatanActivity::class.java)
                        .putExtra(Constants.INTENT_RENJA_NAME, renja.nama)
                        .putExtra(Constants.INTENT_RENJA_ID, renja.id)
                )
            }

            binding.buttonLaporan.setOnClickListener {
                onItemClick?.invoke(renja)
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        private fun bindByProgjaId(programKerjaId: Int): Array<Int> {
            return when (programKerjaId) {
                1 -> arrayOf(R.drawable.img_pembentukan, R.drawable.bg_gradient_red)
                3 -> arrayOf(R.drawable.img_pengawasan, R.drawable.bg_gradient_green)
                2 -> arrayOf(R.drawable.img_penganggaran, R.drawable.bg_gradient_orange)
                else -> arrayOf(R.drawable.img_peningkatan, R.drawable.bg_gradient_blue)
            }
        }
    }
}