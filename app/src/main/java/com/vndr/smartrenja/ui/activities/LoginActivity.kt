package com.vndr.smartrenja.ui.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.widget.doAfterTextChanged
import androidx.transition.TransitionManager
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivityLoginBinding
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.extention.toast
import com.vndr.smartrenja.util.setOnSafeClickListener
import com.vndr.smartrenja.util.startAtTop
import com.vndr.smartrenja.viewmodel.LoginViewModel

class LoginActivity : AppCompatActivity() {
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var binding: ActivityLoginBinding

    private val mainSet by lazy {
        ConstraintSet().apply {
            clone(binding.root)
            setVisibility(binding.loadingGroup.id, ConstraintSet.GONE)
        }
    }

    private val loadingSet by lazy {
        ConstraintSet().apply {
            clone(binding.root)
            setVisibility(binding.loadingGroup.id, ConstraintSet.VISIBLE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.loginLiveData.observe(this, {
            TransitionManager.beginDelayedTransition(binding.root)
            when (it) {
                is Resource.Loading -> {
                    loadingSet.applyTo(binding.root)
                }
                is Resource.Success -> if (it.data != null) viewModel.saveToken()
                is Resource.Error -> {
                    mainSet.applyTo(binding.root)
                    toast { it.getStringMessage() }
                }
            }
        })

        viewModel.tokenLiveData.observe(this, {
            when (it) {
                is Resource.Loading -> {
                    loadingSet.applyTo(binding.root)
                }
                is Resource.Success -> startActivity(
                    Intent(
                        this,
                        MainActivity::class.java
                    ).startAtTop()
                )
                is Resource.Error -> {
                    mainSet.applyTo(binding.root)
                    toast { it.getStringMessage() }
                }
            }
        })

        binding.usernameInput.doAfterTextChanged { enableLogin() }
        binding.passwordInput.doAfterTextChanged { enableLogin() }
        binding.loginButton.setOnSafeClickListener {
            viewModel.login(
                binding.usernameInput.text.toString(),
                binding.passwordInput.text.toString(),
                binding.rememberCheckBox.isChecked
            )
        }
    }

    private fun enableLogin() {
        binding.loginButton.isEnabled = !binding.usernameInput.text.isNullOrBlank() and
                !binding.passwordInput.text.isNullOrBlank()

        binding.loginButton.isEnabled.apply {
            if (this) binding.loginButton.setTextColor(getColor(R.color.white)) else {
                binding.loginButton.setTextColor(getColor(R.color.black))
            }
        }
    }
}