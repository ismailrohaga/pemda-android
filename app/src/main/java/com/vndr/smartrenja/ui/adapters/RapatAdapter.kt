package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemEventBinding
import com.vndr.smartrenja.domain.model.Rapat
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.layoutInflater
import com.vndr.smartrenja.ui.activities.KegiatanDetailActivity
import com.vndr.smartrenja.util.TimeConverter
import java.util.*

class RapatAdapter : RecyclerView.Adapter<RapatAdapter.EventsViewHolder>(), Filterable {
    val data = mutableListOf<Rapat>()
    var filterList = mutableListOf<Rapat>()

    init {
        filterList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        return EventsViewHolder(
            ItemEventBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: EventsViewHolder, position: Int) {
        val data = filterList[position]
        viewHolder.bind(data)
        if (position == filterList.lastIndex) {
            val params = viewHolder.itemView.layoutParams as RecyclerView.LayoutParams
            params.bottomMargin = 32
            viewHolder.itemView.layoutParams = params
        }
    }

    override fun getItemCount(): Int = filterList.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString().toLowerCase(Locale.ROOT).replace(" ", "")
                filterList = if (charSearch.isEmpty()) {
                    data
                } else {
                    val resultList = mutableListOf<Rapat>()

                    data.filter {
                        it.temaRapat?.toLowerCase(Locale.ROOT)?.replace(" ", "")
                            ?.contains(charSearch) == true
                    }.apply {
                        resultList.addAll(this)
                    }

                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = filterList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterList = results?.values as MutableList<Rapat>
                notifyDataSetChanged()
            }

        }
    }

    @SuppressLint("SetTextI18n")
    inner class EventsViewHolder(private val binding: ItemEventBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bind(rapat: Rapat) {
            binding.textViewEventTitle.text = rapat.temaRapat
            binding.textViewEventContent.text = "Rapat"
            binding.textViewEventDate.text =
                rapat.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.imageViewEvent.background =
                binding.root.context.getDrawable(R.drawable.img_rapat)
            binding.constraintLayoutEvent.background =
                binding.root.context.getDrawable(R.drawable.bg_gradient_red)

            binding.root.setOnClickListener {
                it.context.startActivity(
                    Intent(it.context, KegiatanDetailActivity::class.java)
                        .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RAPAT)
                        .putExtra(Constants.INTENT_KEGIATAN_ID, rapat.id)
                        .putExtra("action_request", "null")
                )
            }
        }
    }
}