package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemEventBinding
import com.vndr.smartrenja.domain.model.Schedule
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.layoutInflater
import com.vndr.smartrenja.ui.activities.KegiatanDetailActivity
import com.vndr.smartrenja.util.TimeConverter
import java.util.*

class ScheduleAdapter : RecyclerView.Adapter<ScheduleAdapter.EventsViewHolder>(), Filterable {
    val events = mutableListOf<Schedule>()
    var filterList = mutableListOf<Schedule>()

    init {
        filterList = events
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        return EventsViewHolder(
            ItemEventBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun onBindViewHolder(viewHolder: EventsViewHolder, position: Int) {
        val data = filterList[position]
        when {
            data.rapatId != null -> {
                viewHolder.bindRapat(data)
            }
            data.kunkerId != null -> {
                viewHolder.bindKunker(data)
            }
            else -> {
                viewHolder.bindReses(data)
            }
        }

        if (position == filterList.lastIndex) {
            val params = viewHolder.itemView.layoutParams as RecyclerView.LayoutParams
            params.bottomMargin = 32
            viewHolder.itemView.layoutParams = params
        }
    }

    override fun getItemCount(): Int = filterList.size

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString().toLowerCase(Locale.ROOT).replace(" ", "")
                filterList = if (charSearch.isEmpty()) {
                    events
                } else {
                    val resultList = mutableListOf<Schedule>()

                    events.filter {
                        it.temaRapat?.toLowerCase(Locale.ROOT)?.replace(" ", "")
                            ?.contains(charSearch) == true || it.temaKunker?.toLowerCase(Locale.ROOT)
                            ?.replace(" ", "")
                            ?.contains(charSearch) == true
                    }.apply {
                        resultList.addAll(this)
                    }

                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = filterList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterList = results?.values as MutableList<Schedule>
                notifyDataSetChanged()
            }

        }
    }

    @SuppressLint("SetTextI18n")
    inner class EventsViewHolder(private val binding: ItemEventBinding) :
        RecyclerView.ViewHolder(binding.root) {
        init {
            itemView.setOnClickListener {
                val data = events[bindingAdapterPosition]
                when {
                    data.rapatId != null -> {
                        it.context.startActivity(
                            Intent(it.context, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RAPAT)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, data.rapatId)
                                .putExtra("action_request", "null")
                        )
                    }
                    data.kunkerId != null -> {
                        it.context.startActivity(
                            Intent(it.context, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.KUNKER)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, data.kunkerId)
                                .putExtra("action_request", "null")
                        )
                    }
                    else -> {
                        it.context.startActivity(
                            Intent(it.context, KegiatanDetailActivity::class.java)
                                .putExtra(Constants.INTENT_KEGIATAN_TYPE, Constants.RESES)
                                .putExtra(Constants.INTENT_KEGIATAN_ID, data.resesId)
                                .putExtra("action_request", "null")
                        )
                    }
                }
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindRapat(schedule: Schedule) {
            binding.textViewEventTitle.text = schedule.temaRapat
            binding.textViewEventContent.text = "Rapat"
            binding.textViewEventDate.text =
                schedule.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.imageViewEvent.background =
                binding.root.context.getDrawable(R.drawable.img_rapat)
            binding.constraintLayoutEvent.background =
                binding.root.context.getDrawable(R.drawable.bg_gradient_red)
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindKunker(schedule: Schedule) {
            binding.textViewEventTitle.text = schedule.temaKunker
            binding.textViewEventContent.text = "Kunker"
            binding.textViewEventDate.text =
                schedule.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.imageViewEvent.background =
                binding.root.context.getDrawable(R.drawable.img_kunker)
            binding.constraintLayoutEvent.background =
                binding.root.context.getDrawable(R.drawable.bg_gradient_orange)
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindReses(schedule: Schedule) {
            binding.textViewEventTitle.text = schedule.dasarReses
            binding.textViewEventContent.text = "Reses"
            binding.textViewEventDate.text =
                schedule.tanggal?.let { TimeConverter.convertDateMin(it) }
            binding.imageViewEvent.background =
                binding.root.context.getDrawable(R.drawable.img_rapat)
            binding.constraintLayoutEvent.background =
                binding.root.context.getDrawable(R.drawable.bg_gradient_red)
        }
    }
}