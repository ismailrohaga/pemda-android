package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ItemGalleryBinding
import com.vndr.smartrenja.domain.model.Gallery
import com.vndr.smartrenja.extention.layoutInflater

class GalleryAdapter(val data: List<Gallery>) : RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {
    var onItemClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemGalleryBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])

        if (position == data.lastIndex) {
            val params = viewHolder.itemView.layoutParams as RecyclerView.LayoutParams
            params.bottomMargin = 32
            viewHolder.itemView.layoutParams = params
        }
    }

    inner class ViewHolder(private val binding: ItemGalleryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: Gallery) {
            binding.imageViewGallery.load(data.photoOri) {
                error(R.drawable.bg_placeholder)
                scale(Scale.FILL)
                transformations(RoundedCornersTransformation(32f))
            }

            itemView.setOnClickListener {
                data.photoOri?.let { it1 -> onItemClick?.invoke(it1) }
            }
        }
    }
}