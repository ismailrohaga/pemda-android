package com.vndr.smartrenja.ui.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import coil.load
import com.vndr.smartrenja.databinding.LayoutGalleryPopupBinding
import com.vndr.smartrenja.extention.makeGone

private const val ARG_PARAM_POPUP = "popup_image"

class PopupFragment : DialogFragment() {
    private lateinit var binding: LayoutGalleryPopupBinding
    private val image by lazy { arguments?.getSerializable(ARG_PARAM_POPUP) as String }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = LayoutGalleryPopupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.popupImage.load(image).apply {
            if (this.isDisposed) binding.progressBar.makeGone()
        }
        binding.cancelBtn.setOnClickListener { dismiss() }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.apply {
            setLayout(MATCH_PARENT, WRAP_CONTENT)
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    companion object {
        @JvmStatic
        fun show(image: String, manager: FragmentManager) = PopupFragment().apply {
            arguments = Bundle().apply { putSerializable(ARG_PARAM_POPUP, image) }
        }.show(manager, PopupFragment::class.java.name)
    }
}