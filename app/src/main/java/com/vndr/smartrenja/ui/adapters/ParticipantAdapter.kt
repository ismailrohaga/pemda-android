package com.vndr.smartrenja.ui.adapters

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.size.Scale
import coil.transform.RoundedCornersTransformation
import com.vndr.smartrenja.databinding.ItemMemberBinding
import com.vndr.smartrenja.domain.model.Peserta
import com.vndr.smartrenja.extention.layoutInflater
import java.util.*

class ParticipantAdapter : RecyclerView.Adapter<ParticipantAdapter.ViewHolder>() {
    private var data: MutableList<Peserta> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMemberBinding.inflate(parent.context.layoutInflater, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(list: List<Peserta>) {
        data.clear()
        for (i in list) {
            data.add(i)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])
    }

    inner class ViewHolder(private val binding: ItemMemberBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: Peserta) {
            binding.textViewMemberName.text = data.namaAnggota
            binding.textViewMemberPosition.text = if (data.isExternal == true) "External" else ""
            binding.imageViewPicture.load(data.photo) {
                scale(Scale.FILL)
                transformations(RoundedCornersTransformation(12f))
            }
        }
    }
}