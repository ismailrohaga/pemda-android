package com.vndr.smartrenja.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.FragmentNotificationBinding
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.extention.toast
import com.vndr.smartrenja.ui.adapters.NotificationAdapter
import com.vndr.smartrenja.viewmodel.MainViewModel

class NotificationFragment : Fragment() {
    private lateinit var binding: FragmentNotificationBinding
    private val mainViewModel: MainViewModel by activityViewModels()

    private lateinit var notificationAdapter: NotificationAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewModel.userLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> resource.data?.let { bindUi(it) }
                is Resource.Error -> {
                    toast { resource.getStringMessage() }
                }
            }
        })

        mainViewModel.notificationListLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> resource.data?.let { notificationAdapter.addAll(it) }
                is Resource.Error -> {
                    toast { resource.getStringMessage() }
                }
            }
        })

        mainViewModel.clearNotificationLiveData.observe(viewLifecycleOwner, { resource ->
            when (resource) {
                Resource.Loading -> {
                }
                is Resource.Success -> notificationAdapter.notifyDataSetChanged()
                is Resource.Error -> {
                    toast { getString(R.string.error_unexpected) }
                }
            }
        })

    }

    private fun bindUi(user: User) {
        notificationAdapter = NotificationAdapter(user)
        binding.recyclerviewNotification.adapter = notificationAdapter

        notificationAdapter.onItemClick = {
            toast { getString(R.string.read_notification) }
            it.id?.let { it1 -> mainViewModel.readNotification(it1) }
        }
    }

    override fun onResume() {
        super.onResume()
        mainViewModel.loadNotifications()
    }
}