package com.vndr.smartrenja.ui.activities

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.vndr.smartrenja.R
import com.vndr.smartrenja.databinding.ActivityProgjaBinding
import com.vndr.smartrenja.domain.util.Constants
import com.vndr.smartrenja.extention.makeGone
import com.vndr.smartrenja.ui.adapters.KegiatanPagerAdapter
import com.vndr.smartrenja.viewmodel.KegiatanViewModel
import com.vndr.smartrenja.viewmodel.ProgjaViewModel

class KegiatanActivity : AppCompatActivity() {

    private val kegiatanViewModel: KegiatanViewModel by viewModels()
    private val progjaViewModel: ProgjaViewModel by viewModels()
    private val binding by lazy { ActivityProgjaBinding.inflate(layoutInflater) }

    private var renjaId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        renjaId = intent.getIntExtra(Constants.INTENT_RENJA_ID, 0)
        binding.viewPagerKegiatan.adapter = KegiatanPagerAdapter(supportFragmentManager)
        binding.tabViewPager.setupWithViewPager(binding.viewPagerKegiatan)

        binding.textViewTitle.text = getString(R.string.detail_kegiatan)

        binding.searchInput.doAfterTextChanged {
            progjaViewModel.searchLiveData.postValue(it.toString())
        }

        binding.imageViewBack.setOnClickListener {
            onBackPressed()
        }

        binding.imageButtonOption.makeGone()

        getData()
    }

    private fun getData() {
        kegiatanViewModel.loadRapat(renjaId)
        kegiatanViewModel.loadKunker(renjaId)
    }
}