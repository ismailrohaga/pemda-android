package com.vndr.smartrenja.viewmodel

import androidx.lifecycle.MutableLiveData
import com.vndr.smartrenja.base.BaseViewModel
import com.vndr.smartrenja.domain.model.Banner
import com.vndr.smartrenja.domain.model.Faqs
import com.vndr.smartrenja.domain.model.Notification
import com.vndr.smartrenja.domain.model.Schedule
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.repo.*
import com.vndr.smartrenja.extention.viewModelInjection
import javax.inject.Inject

class MainViewModel : BaseViewModel() {

    @Inject
    lateinit var bannerRepository: BannerRepository

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var kegiatanRepository: KegiatanRepository

    @Inject
    lateinit var notificationRepository: NotificationRepository

    @Inject
    lateinit var faqsRepository: FaqsRepository

    val bannersLiveData by lazy { MutableLiveData<Resource<List<Banner>>>() }
    val userLiveData by lazy { MutableLiveData<Resource<User>>() }
    val logoutLiveData by lazy { MutableLiveData<Resource<Boolean>>() }
    val scheduleListLiveData by lazy { MutableLiveData<Resource<List<Schedule>>>() }
    val upcomingScheduleListLiveData by lazy { MutableLiveData<Resource<List<Schedule>>>() }
    val notificationListLiveData by lazy { MutableLiveData<Resource<List<Notification>>>() }
    val clearNotificationLiveData by lazy { MutableLiveData<Resource<Nothing>>() }
    val faqsListLiveData by lazy { MutableLiveData<Resource<List<Faqs>>>() }
    val termsListLiveData by lazy { MutableLiveData<Resource<List<Faqs>>>() }

    init {
        viewModelInjection.inject(this)
    }

    fun loadBanners() = compositeDisposable.add(
        bannerRepository.getBanner().subscribeUsing(bannersLiveData)
    )

    fun getUser() = compositeDisposable.add(
        userRepository.fetchUser().subscribeUsing(userLiveData)
    )

    fun fetchUser() = compositeDisposable.add(
        userRepository.getUserLocal().subscribeUsing(userLiveData)
    )

    fun logout() = compositeDisposable.add(
        userRepository.logout().subscribeUsing(logoutLiveData)
    )

    fun loadSchedules(akd: Int?, progja: Int?, tahun: Int?) = compositeDisposable.add(
        kegiatanRepository.getSchedule(akd, progja, tahun).subscribeUsing(scheduleListLiveData)
    )

    fun loadUpcomingSchedules() = compositeDisposable.add(
        kegiatanRepository.getUpcomingSchedule().subscribeUsing(upcomingScheduleListLiveData)
    )

    fun loadNotifications() = compositeDisposable.add(
        notificationRepository.getNotifications().subscribeUsing(notificationListLiveData)
    )

    fun clearNotifications() = compositeDisposable.add(
        notificationRepository.clearNotification().subscribeUsing(clearNotificationLiveData)
    )

    fun readNotification(id: String) = compositeDisposable.add(
        notificationRepository.readNotifications(id).subscribeUsing(clearNotificationLiveData)
    )

    fun loadFaqs() = compositeDisposable.add(
        faqsRepository.getFaqs().subscribeUsing(faqsListLiveData)
    )

    fun loadTerms() = compositeDisposable.add(
        faqsRepository.getTerms().subscribeUsing(termsListLiveData)
    )

}