package com.vndr.smartrenja.viewmodel

import androidx.lifecycle.MutableLiveData
import com.vndr.smartrenja.base.BaseViewModel
import com.vndr.smartrenja.domain.model.*
import com.vndr.smartrenja.domain.source.model.User
import com.vndr.smartrenja.domain.source.model.state.Resource
import com.vndr.smartrenja.domain.source.repo.*
import com.vndr.smartrenja.extention.viewModelInjection
import javax.inject.Inject

class ProgjaViewModel : BaseViewModel() {

    @Inject
    lateinit var kegiatanRepository: KegiatanRepository

    @Inject
    lateinit var approvalRepository: ApprovalRepository

    val searchLiveData by lazy { MutableLiveData<String>() }
    val renjaLiveData by lazy { MutableLiveData<Resource<List<Renja>>>() }
    val scheduleLiveData by lazy { MutableLiveData<Resource<List<Schedule>>>() }
    val approveLiveData by lazy { MutableLiveData<Resource<Nothing>>() }

    init {
        viewModelInjection.inject(this)
    }

    fun loadRenja(progja: Int, akd: Int, tahun: Int) = compositeDisposable.add(
        kegiatanRepository.getRenja(progja, akd, tahun).subscribeUsing(renjaLiveData)
    )

    fun loadSchedule(progja: Int, akd: Int, tahun: Int) = compositeDisposable.add(
        kegiatanRepository.getSchedule(progja, akd, tahun).subscribeUsing(scheduleLiveData)
    )

    fun approve(id: Int) = compositeDisposable.add(
        approvalRepository.approveLaporanProgja(id).subscribeUsing(approveLiveData)
    )

    fun reject(id: Int, message: String) = compositeDisposable.add(
        approvalRepository.rejectLaporanProgja(id, message).subscribeUsing(approveLiveData)
    )

}