package com.vndr.smartrenja.util

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object GsonUtils {
    val INSTANCE: Gson = Gson()
}

fun <T> T.toJson(): String {
    return GsonUtils.INSTANCE.toJson(this)
}

@Suppress("unused")
inline fun <reified T> String.fromJson(): T {
    return GsonUtils.INSTANCE.fromJson(this, T::class.java)
}

inline fun <reified T> fromJsonToken(json: String?): T {
    return Gson().fromJson<T>(json, object: TypeToken<T>(){}.type)
}