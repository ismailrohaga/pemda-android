package com.vndr.smartrenja.extention

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.widget.ImageView
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import coil.size.Scale
import coil.util.CoilUtils
import okhttp3.OkHttpClient

fun ImageView.loadSvg(url: String, onRequest: (ImageRequest.Builder.() -> ImageRequest.Builder?)? = null) {
    val imageLoader = ImageLoader.Builder(this.context)
        .componentRegistry { add(SvgDecoder(this@loadSvg.context)) }
        .okHttpClient {
            OkHttpClient.Builder()
                .cache(CoilUtils.createDefaultCache(this.context))
                .build()
        }
        .build()

    val request = ImageRequest.Builder(this.context)
        .data(url)
        .scale(Scale.FILL)
        .target(this)

    if (onRequest != null) {
        request.onRequest()
    }

    imageLoader.enqueue(request.build())
}

fun String.loadImageInTargetListener(context: Context,widthInDp: Int? = null,heightInDp: Int? = null, placeholder: Drawable? = null, onTarget: (drawable : Drawable) -> Unit){
    val imageLoader = ImageLoader.Builder(context)
        .componentRegistry { add(SvgDecoder(context)) }
        .okHttpClient {
            OkHttpClient.Builder()
                .cache(CoilUtils.createDefaultCache(context))
                .build()
        }
        .build()
    if (widthInDp != null && heightInDp != null  ){
        val request = ImageRequest.Builder(context)
            .data(this)
            .placeholder(placeholder)
            .size(widthInDp.dpToPixelsInt(context), heightInDp.dpToPixelsInt(context))
            .target { drawable ->
                onTarget(drawable)
            }
            .build()
        imageLoader.enqueue(request)
    }else{
        val request = ImageRequest.Builder(context)
            .data(this)
            .target { drawable ->
                onTarget(drawable)
            }
            .build()
        imageLoader.enqueue(request)
    }


}

// extension function to convert dp to equivalent pixels
// this method return float value
fun Int.dpToPixels(context: Context):Float = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,this.toFloat(),context.resources.displayMetrics
)


// extension function to convert dp to equivalent pixels
// this method return integer value
fun Int.dpToPixelsInt(context: Context):Int = dpToPixels(context).toInt()
