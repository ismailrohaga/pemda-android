package com.vndr.smartrenja.extention

import android.net.Uri
import java.util.regex.Pattern

fun String.isEmail(): Boolean = Pattern
    .compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"
    ).matcher(this.trim()).matches()

fun String.isPassword(): Boolean = Pattern
    .compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[|!<>@#$%^&+=/*`~_-])(?=\\S+$).{8,}$")
    .matcher(this).matches()

fun String.isFullName(): Boolean = Pattern.compile("^(?=[\\p{L} .'-]+$).{6,}$")
    .matcher(this).matches()

fun String.isValid(regex: String) = regex.toRegex().matches(this)

fun Uri?.getQueryParameterAsString(key: String): String? {
    val valueList = this?.getQueryParameters(key)
    return valueList.let {
        if (valueList.isNullOrEmpty())
            this?.getQueryParameter(key)
        else it?.joinToString()
    }
}

/** this method for encoding url to UTF-8 only query parameters
 * you can add replace method with other special char that invalid for webview
 * for ref: https://developers.google.com/maps/documentation/urls/url-encoding
 * */
fun String.encodeValidUrl(): String =
    this.replace("|", "%7C") // this replace to solve issue on link from payment
        .replace(" ", "%20")

fun String.containsInvalidCharacter(): Boolean {
    if (this.contains("|")) return true
    if (this.contains(" ")) return true
    return false
}



